<?php
use PHPUnit\Framework\TestCase;

final class SignupTest extends TestCase{
    public function testUsername() {
	$username = 'kamel';
	$this->assertNotEmpty($username, "username is not empty");
    }

    public function testPassword() {
	$password = '123456';
	$password2 = '123456';
	$this->assertEquals($password,$password, "password is equals");
	
    }
    public function testEmail() {
	$email = 'vtc17017611@gmail.com';
	$this->assertRegExp('/^.+\@\S+\.\S+$/', $email, "this is Email format");
    }
    

    
}
