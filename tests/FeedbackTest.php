<?php
use PHPUnit\Framework\TestCase;

final class FeedbackTest extends TestCase{
    public function testFeedbackNotEmpty(){
	$feedback = 'goods';
	$this->assertNotEmpty($feedback, "feedback is not empty");
    }
    public function testUserNameNotEmpty(){
	$username = 'kamel';
	$this->assertNotEmpty($username, "username is not empty");
    }

    public function testItemIDNotEmpty(){
	$item_id = '1';
	$this->assertNotEmpty($item_id, "item_id is not empty");
    }

    
}
