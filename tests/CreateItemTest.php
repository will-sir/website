<?php
use PHPUnit\Framework\TestCase;

final class CreateItemTest extends TestCase{
    public function testProductNameNotEmpty() {
	$ProductionName = "M.2 SSD";
	$this->assertNotEmpty($ProductionName, "Product name is not Empty");
    }
    public function testDescriptionNotEmpty() {
	$Description = '100% new M.2 SSD did not openit';
	$this->assertNotEmpty($Description, "Description did not Empty");
    }
    public function testLocationNotEmpty() {
	$location = "Sham Shui Po MTR exit C2";
	$this->assertNotEmpty($location, "Location did not Empty");
    }

    public function testItemPrict() {
	$price = "700";
	$this->assertNotEmpty($price, "price did not Empty");
    }


    public function testPhoneNumber() {
	$PhoneNumber = "22567000";
	$this->assertNotEmpty($PhoneNumber, "Phone number did not Empty");
    }
    
}
