<?php include_once('uploaditem.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<div class="navbar">
  <div class="logo">
  <img src="login/shopping.png" class="icon">
  <div class="name">Shopping online</div> 
</div>
<div class="menu">
  <ul>
    <li><span id="username"></span></li>
    <li><a href="index.php">Home</a></li>
    <li ><a href="CreateItem.php">Create Item</a></li>
    <li>Testing</li>
    <li><a href="login/logout.php">Logout</a></li>
</ul>
    
</div>


<form action="CreateItem.php" method="post" enctype="multipart/form-data">
   <div class="textbox">
     <div class="style">
   <p> Product Image</p>  
		<input type = "file" name = "product_image" id ="product_image" class="form-control">
    <br>
    <p> Product Name</p> 
		<input type="text" name="product_name" placeholder=" Item name">
    <input type="hidden" name="username" id="user">	
    <br>
		<p> Description</p> 
    <textarea name = "description" id ="description" class="form-control"> </textarea>
		<p> Location</p> 
    <textarea name = "location" id ="location" class="form-control"> </textarea>
    <br>
    <p> Item Price</p>
		<input type="text" name="product_price" id="price" placeholder=" Item price "><p>
    <br>
		<p> Phone number</p>
		<input type="text" id="phone" name="phone" placeholder=" phone number "><p>
      <br><br><br>
    <button type = "submit" id="submit" name="save-user" class = "btn">Upload</button>
     </form>
        </div>
</div>



</body>

<link rel="stylesheet" href="css/CreateItem.css">
 <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src= "login/js/CreateItem.js"> </script>




</html>
