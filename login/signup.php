<?php
  session_start();

  if (isset($_SESSION['username'])) {
      session_start();
      session_destroy();
  }


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Signup</title>
    <link rel="stylesheet" href="signup.css">
  </head>

  <body>
  <div class="login-box">
  <img src="shopping.png" class="icon">
        <h1>Sign up</h1> 
        <div class="textbox">
        <p>Username</p>
        <img src="user.png">
        <input name="newuser" id="newuser" type="text" placeholder="Username" autofocus>
        </div>

        <div class="textbox">  
        <p>Email</p> 
        <img src="mail.jpg">
        <input name="email" id="email" type="text" placeholder="Email">
        </div>

        <div class="textbox">  
        <p>Pasword</p>  
        <img src="key.png">
        <input name="password1" id="password1" type="password" placeholder="Password">
        <toggle onclick="myFunction()" class="show"><img src="show.png">
        </div>

        <div class="textbox">  
        <p>Repeat Pasword</p>
        <img src="key.png">
        <input name="password2" id="password2" type="password" placeholder="Repeat Password">
        <toggle onclick="Function()" class="show"><img src="show.png">
        </div>

        <button name="Submit" id="submit" class="btn" type="submit">Sign up</button>
        <button class="btn"><a href="main_login.php">Sign In page</a></button>
        <div id="message"></div>
      </form>

    </div> <!-- /container -->

    <script>
    function myFunction() {
    var x = document.getElementById("password1");
    if (x.type === "password") {
    x.type = "text";
    } else {
    x.type = "password";
    }

    
    } 
    </script>
     <script>
    function Function() {
    var x = document.getElementById("password2");
    if (x.type === "password") {
    x.type = "text";
    } else {
    x.type = "password";
    }
    } 
    </script>


</script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <script src="js/signup.js"></script>


    <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>

$( "#usersignup" ).validate({
  rules: {
	email: {
		email: true,
		required: true
	},
    password1: {
      required: true,
      minlength: 4
	},
    password2: {
      equalTo: "#password1"
    }
  }
});
</script>

  </body>
</html>
