<?php
session_start();
if (isset($_SESSION['username'])) {
    header("location:../index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Login</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
  <div class="login-box">
  <img src="shopping.png" class="icon">
        <h1>Login</h1> 
    <div class="textbox">
    <p>Username</p>
        <img src="user.png">
        <input name="myusername" id="myusername" type="text"  placeholder="Username">
        </div>
        <div class="textbox">  
        <p>Password</p>  
        <img src="key.png">
        <input name="mypassword" id="mypassword" type="password" placeholder="Password">
        <toggle onclick="myFunction()" class="show"><img src="show.png">
        </div>
        
        <button name="Submit" id="submit" class="btn" type="submit">Sign in</button>
        <a href="#">Lost your password?</a><br>
        <a href="signup.php">Don't have an account?</a>
        <div id="message"></div>
</div>
      </form>


      <script>
    function myFunction() {
    var x = document.getElementById("mypassword");
    if (x.type === "password") {
    x.type = "text";
    } else {
    x.type = "password";
    } 
    } 
    </script>

 <script src="js/jquery-2.2.4.min.js"></script>
   
    <script type="text/javascript" src="js/bootstrap.js"></script> 
    <script src="js/login.js"></script> 

  </body>
</html>